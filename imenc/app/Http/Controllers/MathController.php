<?php

namespace App\Http\Controllers;

use illuminate\Http\Request;

class MathController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function genap1(Request $request) {
        $angka = 0;
        for ($i=0; $i<=4; $i++){
            $angka = $angka + 2;
            echo $angka;
        }
    }

    public function arraytest(Request $request) {
        $database[0]['nama'] = 'qholis';
        $database[1]['nama'] = 'ikhwan';
        $database[2]['nama'] = 'kris';

        $database[0]['alamat'] = 'jogja';
        $database[1]['alamat'] = 'sleman';
        $database[2]['alamat'] = 'bantul';

        $alamat = request()->query('alamat');


        echo 'alamat : '. $alamat;
        echo '<br>';

        // print_r($database);

        foreach ($database as $index=>$value){
            if ($value['alamat'] == $alamat) {
                echo 'nama : '. $value['nama'];
            }
        }
    }

    public function triangle1(Request $request){
        $nomor = (int)request()->query('nomor');
        for($x=0; $x<=$nomor; $x++){
            for($y=1; $y<=$x; $y++){
                echo 'o';
            }
            echo '<br>';
        }
        for($x=0; $x<=$nomor; $x++){
            for($y=(int)$nomor-$x; $y>1; $y--){
                echo'o';
            }
            echo '<br>';
        }
    }

    public function triangle(Request $request){
        $nomor = request()->query('nomor');
        for($x=0; $x<=(int)$nomor; $x++){
            for($y=1; $y<=$x; $y++){
                echo 'o';
            }
            echo '<br>';
        }
        for($x=0; $x<=(int)$nomor; $x++){
            for($y=(int)$nomor-$x; $y>1; $y--){
                echo'o';
            }
            echo '<br>';
        }
    }
}
